package br.com.mastertech.buscaenderecousuario;

import br.com.mastertech.buscaenderecousuario.clients.BuscaCepClient;
import br.com.mastertech.buscaenderecousuario.clients.EnderecoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BuscaController {

    @Autowired
    BuscaCepClient buscaCepClient;

    @GetMapping("buscaendusr/{nome}/{cep}")
    public EnderecoUsrDTO receber(@PathVariable String nome, @PathVariable String cep) {
        EnderecoUsrDTO enderecoUsrDTO = new EnderecoUsrDTO();
        EnderecoDTO enderecoDTO = buscaCepClient.enderecoDTO(cep);
        enderecoUsrDTO.setNome(nome);
        enderecoUsrDTO.setEndereço(enderecoDTO.getLogradouro() + " - " + enderecoDTO.getComplemento() +  " - " + enderecoDTO.getBairro() + " - " +
                enderecoDTO.getUf());
        return enderecoUsrDTO;
    }
}
