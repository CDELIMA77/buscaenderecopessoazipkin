package br.com.mastertech.buscaenderecousuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BuscaenderecousuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuscaenderecousuarioApplication.class, args);
	}

}
